const axios = require('axios');
exports.handler = function (event, context, callback) {
    var options = {
        method: 'POST',
        url: 'https://circleci.com/api/v1.1/project/github/supplyaidev/cypress-automate',
        headers:
        {
            'cache-control': 'no-cache',
            Connection: 'keep-alive',
            'Content-Length': '0',
            'Accept-Encoding': 'gzip, deflate',
            'Cache-Control': 'no-cache',
            Accept: '*/*',
            'Circle-Token': '6a97adb6c91b7f62959f9cfcf33b4c5ecf659e03'
        }
    };
    axios(options).then(data => {
        callback(null, {
            statusCode: 200,
            body: JSON.stringify({
                message: "Deployed successfully",
            })
        })
    }).catch(err => {
        callback(null, {
            statusCode: 400,
            body: JSON.stringify({ "message": err })
        })
    })


}